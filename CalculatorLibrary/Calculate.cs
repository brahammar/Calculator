﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculatorLibrary
{
    class Calculate
    {
        internal bool UseParentheses { get; }

        internal Calculate(bool useParentheses = false)
        {
            UseParentheses = useParentheses;
        }

        internal double PerfomeCalculation(string valueString)
        {
            return PerfomeCalculation(_GenerateListFromString(valueString));
        }

        internal double PerfomeCalculation(List<string> valueList)
        {
            _PrepareNegativeValues(ref valueList);
            _ValidateValues(ref valueList);

            if (UseParentheses)
            {
                var parentheseInfo = new ParentheseInfo(valueList);

                if (parentheseInfo.IsParenthesesMatching)
                {
                    valueList = _ExtractAndCalculateParentheses(valueList, parentheseInfo);
                }
            }

            return _DoCalculations(valueList);
        }

        internal bool IsStringOperand(string s)
        {
            switch (s)
            {
                case "+":
                    return true;
                case "-":
                    return true;
                case "*":
                    return true;
                case "/":
                    return true;
                case "(":
                    return UseParentheses;
                case ")":
                    return UseParentheses;
                default:
                    return false;
                    //throw new Exception($"{s} is not a valid operand.");
            }
        }

        internal bool IsStringValidInOperation(string s, double minValue = double.MinValue, double maxValue = double.MaxValue)
        {
            double tempDouble;

            return IsStringOperand(s)
                || double.TryParse(s, out tempDouble)
                && (tempDouble >= minValue && tempDouble <= maxValue);
        }

        private List<string> _GenerateListFromString(string valueString)
        {
            List<string> list = new List<string>();
            string tempString = "";
            double tempDouble;

            foreach (var s in valueString)
            {
                if (s == ' ')
                    continue;

                if (double.TryParse(s.ToString(), out tempDouble))
                {
                    tempString += tempDouble;
                }
                else if (IsStringOperand(s.ToString()))
                {
                    if (tempString.Length > 0)
                    {
                        list.Add(tempString);
                        tempString = "";
                    }

                    list.Add(s.ToString());
                }
            }

            if (tempString.Length > 0)
                list.Add(tempString);

            return list;
        }

        private void _PrepareNegativeValues(ref List<string> valueList)
        {
            var previusValue = "";

            for (int i = 0; i < valueList.Count; i++)
            {
                double nextValueNumber = 0;
                bool nextValueIsNumber = (i + 1 < valueList.Count) && double.TryParse(valueList[i + 1], out nextValueNumber);

                if (valueList[i] == "-" && (previusValue == "" || IsStringOperand(previusValue)) && nextValueIsNumber)
                {
                    valueList.RemoveAt(i);
                    valueList[i] = (nextValueNumber * -1).ToString();
                }
                else if (valueList[i] == "-" && nextValueIsNumber)
                {
                    valueList[i] = "+";
                    valueList[i + 1] = (nextValueNumber * -1).ToString();
                }

                previusValue = valueList[i];
            }
        }

        private double _DoCalculations(List<string> valueList)
        {
            while (valueList.Count > 2)
            {
                int index = _GetNextOperatorIndex(valueList);

                _ExtractAndCalculateAtIndex(index, ref valueList);
            }

            double sum;
            if (double.TryParse(valueList[0], out sum))
                return sum;
            else
                throw new Exception($"The sum was not a valid number ({valueList[0]})");
        }

        private int _GetNextOperatorIndex(List<string> valueList)
        {   // TODO: Fixa prioritering.
            int operatorIndex = -1;

            for (int i = 0; i < valueList.Count; i++)
            {
                if (valueList[i] == "*" || valueList[i] == "/")
                {
                    operatorIndex = i;
                    break;
                }

                if (
                    operatorIndex == -1
                    && (valueList[i] == "+" || valueList[i] == "-"))
                {
                    operatorIndex = i;
                }
            }

            return operatorIndex;
        }

        private void _ExtractAndCalculateAtIndex(int index, ref List<string> valueList)
        {
            string operand = valueList[index];

            double valueOne;
            if (!double.TryParse(valueList[index - 1], out valueOne))
                throw new Exception($"{valueList[index]} is not a valid number.");

            double valueTwo;
            if (!double.TryParse(valueList[index + 1], out valueTwo))
                throw new Exception($"{valueList[index]} is not a valid number.");

            switch (operand)
            {
                case "+":
                    valueList[index - 1] = _Add(valueOne, valueTwo).ToString();
                    break;
                case "-":
                    valueList[index - 1] = _Subtract(valueOne, valueTwo).ToString();
                    break;
                case "*":
                    valueList[index - 1] = _Multiply(valueOne, valueTwo).ToString();
                    break;
                case "/":
                    valueList[index - 1] = _Divide(valueOne, valueTwo).ToString();
                    break;
                default:
                    throw new Exception($"{operand} is not a valid operand.");
            }

            valueList.RemoveAt(index);
            valueList.RemoveAt(index);
        }

        private double _Add(params double[] numbers)
        {
            var sum = 0d;

            foreach (double d in numbers)
            {
                sum += d;
            }

            return sum;
        }

        private double _Subtract(params double[] numbers)
        {
            double sum;

            if (numbers.Length > 1)
                sum = numbers[0];
            else
                throw new Exception("Can not subtract with only one number.");

            for (int i = 1; i < numbers.Length; i++)
            {
                sum -= numbers[i];
            }

            return sum;
        }

        private double _Multiply(params double[] numbers)
        {
            var sum = 0d;

            if (numbers.Length != 0)
                sum = 1;

            foreach (double d in numbers)
            {
                sum *= d;
            }

            return sum;
        }

        private double _Divide(params double[] numbers)
        {
            if (Array.FindIndex(numbers, item => item == 0d) > 0)
            {
                throw new Exception("Can not divide by zero.");
            }

            double sum;

            if (numbers.Length > 1)
                sum = numbers[0];
            else
                throw new Exception("Can not devid with only one number.");

            for (int i = 1; i < numbers.Length; i++)
            {
                sum /= numbers[i];
            }

            return sum;
        }

        private List<string> _ExtractAndCalculateParentheses(List<string> valueList, ParentheseInfo parentheseInfo)
        {
            return valueList = _CalculateParenthesesRecursevly(valueList, parentheseInfo);
        }

        private List<string> _CalculateParenthesesRecursevly(List<string> valueList, ParentheseInfo parentheseInfo)
        {
            valueList[parentheseInfo.Positions[0].OpeningIndex] = _DoCalculations(
                valueList.Where(
                (value, index) =>
                    index > parentheseInfo.Positions[0].OpeningIndex &&
                    index < parentheseInfo.Positions[0].ClosingIndex).ToList()
                ).ToString();

            valueList =
                valueList.Where(
                    (value, index) =>
                        index <= parentheseInfo.Positions[0].OpeningIndex ||
                        index > parentheseInfo.Positions[0].ClosingIndex
                ).ToList();

            parentheseInfo = new ParentheseInfo(valueList);

            if (parentheseInfo.IsParenthesesMatching)
                valueList = _CalculateParenthesesRecursevly(valueList, parentheseInfo);

            return valueList;
        }

        private List<string> _AddMissingOperatorsOnParentheses(List<string> valueList)
        {
            _AddMissingOperatorsOnOpeingParentheses(ref valueList);
            _AddMissingOperatorsOnClosingParentheses(ref valueList);

            return valueList;
        }

        private void _AddMissingOperatorsOnOpeingParentheses(ref List<string> valueList, int startIndex = 1)
        {
            int position = valueList.IndexOf("(", startIndex);

            if (position > 0 && !IsStringOperand(valueList[position - 1]))
                valueList.Insert(position, "*");

            if (position > 0)
                _AddMissingOperatorsOnOpeingParentheses(ref valueList, position + 1);
        }

        private void _AddMissingOperatorsOnClosingParentheses(ref List<string> valueList, int startIndex = 1)
        {
            int position = valueList.IndexOf(")", startIndex);

            if (position > 0 && position < valueList.Count - 1 && !IsStringOperand(valueList[position + 1]))
                valueList.Insert(position + 1, "*");

            if (position > 0 && position != valueList.Count)
                _AddMissingOperatorsOnClosingParentheses(ref valueList, position + 1);
        }

        private bool _ValidateValues(ref List<string> valueList)
        {
            if (!_ValidateValuesStart(ref valueList) || !_ValidateValuesEnd(ref valueList) || valueList.Count < 3)
                return false;

            if (UseParentheses)
                valueList = _AddMissingOperatorsOnParentheses(valueList);

            var previusValueOperand = false;

            for (int i = 0; i < valueList.Count; i++)
            {

                if (valueList[i] != ")" && IsStringOperand(valueList[i]))
                {
                    if (previusValueOperand && valueList[i] != "(")
                    {
                        throw new Exception($"{valueList[i - 1]} folowed by {valueList[i]} is not a valid combination in calculation.");
                    }

                    previusValueOperand = true;
                }
                else
                {
                    previusValueOperand = false;

                }
            }

            return true;
        }

        private bool _ValidateValuesStart(ref List<string> valueList)
        {
            if (valueList.Count > 0 && (valueList[0] != "(" && IsStringOperand(valueList[0])))
                //valueList.RemoveAt(0);
                throw new Exception($"{valueList[0]} is not a valid symbol at start of equation.");

            return true;
        }

        private bool _ValidateValuesEnd(ref List<string> valueList)
        {
            if (valueList.Count > 0 && (valueList[valueList.Count - 1] != ")" && IsStringOperand(valueList[valueList.Count - 1])))
                throw new Exception($"{valueList[valueList.Count - 1]} is not a valid symbol at end of equation.");

            return true;
        }
    }
}
