﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.System.UserProfile;

namespace CalculatorLibrary
{
    class ParentheseInfo
    {
        internal bool IsParenthesesMatching
        {
            get { return (Positions.Count > 0); }
        }

        internal List<Parenthes> Positions { get; private set; } = new List<Parenthes>();

        internal ParentheseInfo(List<string> values)
        {
            _ExtractPositionsFromList(values);
        }

        private bool _ExtractPositionsFromList(List<string> valueList)
        {
            var openingParentheses = valueList.Count(value => value == "(");
            var closingParentheses = valueList.Count(value => value == ")");

            if (openingParentheses != closingParentheses)
            {
                //return false;
                throw new Exception($"Uneaven amount of parentheses, {openingParentheses} opening parentheses and {closingParentheses} closing parentheses.");
            }

            _GetMatchingParenthesSet(ref valueList);

            Positions.Reverse();

            return IsParenthesesMatching;
        }

        private void _GetMatchingParenthesSet(ref List<string> valueList, int startIndex = 0)
        {
            var firstParenthes = -1;
            var matchingParenthes = -1;
            var numberOfOpeningParentheses = 0;
            var numberOfClosingParentheses = 0;

            for (int i = startIndex; i < valueList.Count; i++)
            {
                if (valueList[i] == "(" && firstParenthes < 0)
                {
                    firstParenthes = i;
                }
                else if (valueList[i] == "(")
                {
                    numberOfOpeningParentheses++;
                }
                else if (firstParenthes >= 0 && valueList[i] == ")" &&
                         numberOfOpeningParentheses == numberOfClosingParentheses)
                {
                    matchingParenthes = i;
                    break;
                }
                else if (firstParenthes >= 0 && valueList[i] == ")" && !Positions.Any(p => p.ClosingIndex == i))
                {
                    numberOfClosingParentheses++;
                }
            }

            if ((firstParenthes >= 0 && matchingParenthes < 0) || (firstParenthes < 0 && matchingParenthes >= 0))
                throw new Exception($"Opening and closing parentheses do not match.");

            if (firstParenthes >= 0 && matchingParenthes >= 0)
                Positions.Add(new Parenthes(firstParenthes, matchingParenthes));

            if (firstParenthes >= 0 && matchingParenthes + 1 <= valueList.Count)
                _GetMatchingParenthesSet(ref valueList, firstParenthes + 1);
        }
    }
}