﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace CalculatorLibrary
{
    internal class Parenthes
    {
        public bool LoadedSuccessfylly { get; private set; }
        internal int OpeningIndex { get; private set; }
        internal int ClosingIndex { get; private set; }

        internal Parenthes(int openingIndex, int closingIndex)
        {
            OpeningIndex = openingIndex;
            ClosingIndex = closingIndex;
        }
    }
}
