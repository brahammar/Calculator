﻿using System;
using System.Globalization;
using CalculatorLibrary;
using System.Linq;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CalculatorApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Calculate _calculateObject;

        private Calculate CalculateObject
        {
            get
            {
                if (_calculateObject != null)
                    return _calculateObject;

                return _calculateObject = new Calculate(useParentheses: true);
            }
        }

        public MainPage()
        {
            InitializeComponent();
        }

        private void textBoxMain_TextChanging(TextBox sender, TextBoxTextChangingEventArgs args)
        {
            try
            {
                TextBox textbox = sender;
                var caretPosition = CalculateNewCratePosition(textbox);

                textBoxMain.Text = string.Concat(textbox.Text.Where(c => CalculateObject.IsStringValidInOperation(c.ToString())));
                textBoxMain.SelectionStart = caretPosition;

                if (
                    textbox.Text.Length > 0 
                    && (textbox.Text.Last().ToString() == ")" || !CalculateObject.IsStringOperand(textbox.Text.Last().ToString()))
                    )
                {
                    textBlockMain.Text = CalculateObject.PerfomeCalculation(textBoxMain.Text).ToString(CultureInfo.CurrentCulture);
                }
                else if (textbox.Text.Length == 0)
                    textBlockMain.Text = string.Empty;
            }
            catch (Exception ex)
            {
                textBlockMain.Text = ex.Message;
            }
        }

        private int CalculateNewCratePosition(TextBox textbox)
        {
            var caretPosition = textbox.SelectionStart;

            if (
                textbox.Text.Length > 0 
                && !CalculateObject.IsStringValidInOperation(textbox.Text.ToCharArray()[caretPosition > 0 ? caretPosition - 1 : caretPosition].ToString())
                )
            {
                caretPosition--;
            }

            return caretPosition;
        }


        private void buttonNumber_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {

        }
    }
}
